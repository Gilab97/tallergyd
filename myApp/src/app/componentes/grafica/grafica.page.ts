import { Component, OnInit, ViewChild  } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import {DatosService} from "../../servicios/datos.service";
import {GlobalService} from "../../servicios/global.service";

@Component({
  selector: 'app-grafica',
  templateUrl: './grafica.page.html',
  styleUrls: ['./grafica.page.scss'],
})
export class GraficaPage implements OnInit {

  public lineChartData: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0], label: 'SERVIDOR 1 CPU' }
  ];

  public lineChartData2: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0], label: 'SERVIDOR 1 RAM' }
  ];

  public lineChartLabels: Label[] = ['1', '2', '3', '4', '5', '6'];

  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };

  public lineChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  public conta: number = 6;
  constructor(private datosService: DatosService,
              private global: GlobalService) { }

  ngOnInit() {
    this.global.fas = 0
    console.log(`Iniciar Server 1 ${this.global.fas}`)
    this.cargar()
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public pushOne(ram, cpu) {

    this.lineChartData.forEach((x, i) => {
      const data: number[] = x.data as number[];
      data.shift();
      data.push(cpu);
    });

    this.lineChartData2.forEach((x, i) => {
      const data2: number[] = x.data as number[];
      data2.shift();
      data2.push(ram);
    });

    this.conta = this.conta + 1
    this.lineChartLabels.shift()
    this.lineChartLabels.push(`${this.conta}`);
  }

  async cargar(){
    var ram = 0.0
    var cpu = 0.0
    await delay(1500);
    this.datosService.getdatos1s1().subscribe(
        res =>{
          if (typeof res === "string") {
            //console.log(JSON.parse(res))
            var val = JSON.parse(res)

            ram = (val['used']/val['ram'])*100

            console.log(`Server 1 RAM ${ram}`)

          }
        },
        error => console.error(error)
    )
    await delay(1500);
    this.datosService.getdatos2s1().subscribe(
        res =>{
          if (typeof res === "string") {
            //console.log(JSON.parse(res))
            var val = JSON.parse(res)

            cpu = 100.005-((val['free']/val['cpu'])*100)

            console.log(`Server 1 CPU ${cpu}`)

          }
        },
        error => console.error(error)
    )
    await delay(1500);
    this.pushOne(ram,cpu)
    await delay(1500);
    console.log(`Server 1 ${this.global.fas}`)
    if (this.global.fas == 1){
      return 0
    }else {
      await this.cargar()
    }
  }

  change(){
    this.global.fas = 1
    console.log(`Detener Server 1 ${this.global.fas}`)
  }

}

function delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}
