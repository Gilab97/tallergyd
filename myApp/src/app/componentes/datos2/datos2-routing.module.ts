import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Datos2Page } from './datos2.page';

const routes: Routes = [
  {
    path: '',
    component: Datos2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Datos2PageRoutingModule {}
