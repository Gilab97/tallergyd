import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Datos2PageRoutingModule } from './datos2-routing.module';

import { Datos2Page } from './datos2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Datos2PageRoutingModule
  ],
  declarations: [Datos2Page]
})
export class Datos2PageModule {}
