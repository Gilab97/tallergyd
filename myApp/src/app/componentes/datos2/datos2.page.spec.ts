import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Datos2Page } from './datos2.page';

describe('Datos2Page', () => {
  let component: Datos2Page;
  let fixture: ComponentFixture<Datos2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Datos2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Datos2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
