import { Component, OnInit } from '@angular/core';
import {GlobalService} from "../../servicios/global.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private global: GlobalService) { }

  ngOnInit() {
    this.global.fas = 1
    this.global.fas2 = 1
    this.global.fas3 = 1
    this.global.fas4 = 1
  }

  change(){
    this.global.fas = 1
    this.global.fas2 = 1
    this.global.fas3 = 1
    this.global.fas4 = 1
  }
}
